# Helper tool for the game [Astrox Imperium](https://astroximperium.com).

![screenshot](screenshot.png)

## Displays station and ship information extracted from the current log files in a sortable table.

**Usage:**<br> 
On a standard steam installation on windows with only one game save you can simply double click the jar file.

Else you start the jar from a command prompt:<br>
`java -jar astroxspy.jar "name of save" (for standard steam install)`
<br>or<br>
`java -jar astroxspy.jar "name of save" "path to game"`

Example:<br> 
`java -jar astroxspy.jar "MySave"`<br>
`java -jar astroxspy.jar "My Save" "C:\Program Files (x86)\Astrox Imperium"`
