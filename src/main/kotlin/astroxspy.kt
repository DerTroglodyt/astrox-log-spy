@file:Suppress("KDocMissingDocumentation")

import java.awt.*
import java.awt.event.*
import java.io.*
import java.text.*
import javax.swing.*
import javax.swing.table.*
import kotlin.system.*

fun List<String>.readProperty(name: String): String =
    this.dropWhile { !it.startsWith("$name;") }.first().substringAfterLast(";")

fun List<String>.readSection(name: String): List<String> {
    var done = false
    return this.dropWhile { !it.startsWith("<$name>") }
        .drop(1)
        .filter { !it.startsWith("//") }
        .mapNotNull {
            if (it.startsWith("<")) {
                done = true
            }
            if (!done && it.isNotEmpty()) {
                it
            } else {
                null
            }
        }
}

fun Int.formated(): String = NumberFormat.getIntegerInstance().format(this)
fun Double.formated(): String = NumberFormat.getIntegerInstance().format(this.toInt())

data class SectorID(val id: String)
data class StationID(val id: String)
data class ShipID(val id: String)
data class ItemID(val id: Int)

data class Sector(
    val id: SectorID,
    val name: String,
    val known: Boolean

) {
    override fun toString(): String {
        return name + if (!known) "(unknown)" else ""
    }
}

private val UNKNOWN_SECTOR = Sector(SectorID("?"), "UNKNOWN SECTOR", false)

data class Ship(
    val id: ShipID,
    val name: String,
    val clazz: String,
    val type: String,
    val price: Int,
    val skillLevel: Int,
    val activeSlots: Int,
    val passiveSlots: Int,
    val cargo: Int
) {
    override fun toString(): String {
        return "'$name' ($clazz $skillLevel, $type) $activeSlots|$passiveSlots|${cargo.formated()}, ${price.formated()}€"
    }
}

private val UNKNOWN_SHIP = Ship(ShipID("-1"), "UNKNOWN SHIP", "?", "?", 0, 0, 0, 0, 0)

data class ShipInHangar(val ship: Ship, val modifier: Double, val count: Int)

enum class StationService {
    UNKNOWN,
    lounge,
    market,
    refinery,
    garage,
    university,
    hangar,
    storage,
    fabricator,
    contracts,
    admin
}

data class Faction(val id: String, val name: String, val score: Double, val bonusses: List<Double>)
private val UNKNOWN_FACTION = Faction("-1", "UNKNOWN FACTION", 0.0, List(18) { 0.0 })

data class Station(
    val id: StationID,
    val name: String,
    val faction: Faction,
    val sector: Sector,
    val known: Boolean,
    val hangar: List<ShipInHangar>,
    val marketSellOrder: List<Item>,
    val demand: List<Item>,
    val marketItems: List<Item>,
    val services: Set<StationService>
) {
    override fun toString(): String {
        return "'$name'" + (if (!known) "(unknown)" else "") + " in '$sector' Hangar: $hangar"
    }
}

private val UNKNOWN_STATION =
    Station(
        StationID("-1"), "UNKNOWN STATION", UNKNOWN_FACTION, UNKNOWN_SECTOR, false, listOf(),
        listOf(), listOf(), listOf(), setOf()
    )

data class Item(
    val id: ItemID,
    val name: String,
    val type: String,
    val clazz: String,
    val subClass: String,
    val size: Double,
    val price: Double,
    val level: Int,
    val stackable: Boolean,
    val description: String,
    val logCount: Int,
    val logPrice: Double
) {
    companion object {
        fun fromStr(s: String): Item {
            val p = s.split(";").map { it.trim() }
            return Item(
                ItemID(p[1].trim().toInt()),
                p[2],
                p[3],
                p[4],
                p[5],
                p[7].trim().toDouble(),
                p[8].toDouble(),
                p[10].trim().toInt(),
                p[12].toBoolean(),
                p[22],
                0,
                0.0
            )
        }
    }

    override fun toString(): String {
        return "Item(name='$name', type='$type', clazz='$clazz', subClass='$subClass', size=$size, price=$price, level=$level, stackable=$stackable, description='$description', logCount=$logCount, logPrice=$logPrice)"
    }
}

private val UNKNOWN_ITEM = Item(
    ItemID(-1), "UNKNOWN ITEM", "", "", "", 0.0, 0.0,
    0, false, "", 0, 0.0
)

class DB(dbPath: String, savePath: String) {
    val factions: List<Faction>
    val sectors: Map<SectorID, Sector>
    val ships: Map<ShipID, Ship>
    val stations: Map<StationID, Station>
    val items: Map<ItemID, Item>

    init {
        factions = File("$savePath\\factions\\factions_database.txt").readLines()
            .filter { it.isNotEmpty() && !it.startsWith("//") }
            .drop(1)
            .map { line ->
                val p = line.split(";")
                val bonusses = p[6].split(",")
                    .map { it.split("#")[1].toDouble() }
                Faction(p[2], p[3], p[4].toDouble(), bonusses)
            }

        sectors = File("$savePath\\sectors").listFiles(FileFilter { it.isFile })
            ?.filterNotNull()
            ?.associate { file ->
                val s = file.readLines()
                val id = SectorID(file.nameWithoutExtension)
                val name = s.readProperty("SECTOR;name")
                id to Sector(id, name, false)
            } ?: throw IllegalStateException()

        ships = File("$dbPath\\ships").listFiles(FileFilter { it.name.endsWith(".txt") })
            ?.filterNotNull()
            ?.associate { file ->
                val s = file.readLines()
                val id = ShipID(file.nameWithoutExtension)
                val name = s.readProperty("SHIP_name")
                val clazz = s.readProperty("SHIP_class")
                val type = s.readProperty("SHIP_type")
                val price = s.readProperty("SHIP_base_price").toInt()
                val skillLevel = s.readProperty("SHIP_skill_level").toInt()
                val activeSlots = s.readProperty("SHIP_active_slots").toInt()
                val passiveSlots = s.readProperty("SHIP_passive_slots").toInt()
                val cargo = s.readProperty("SHIP_base_cargo").toDouble().toInt()
                id to Ship(id, name, clazz, type, price, skillLevel, activeSlots, passiveSlots, cargo)
            } ?: throw IllegalStateException()

        stations = File("$savePath\\stations").listFiles(FileFilter { it.isFile })
            ?.filterNotNull()
            ?.associate { file ->
                val sectorIdStr = file.nameWithoutExtension
                val sectorId = SectorID(sectorIdStr.substringBeforeLast("_").substringBeforeLast("_"))
                val id = StationID(file.nameWithoutExtension)
                val s = file.readLines()
                val name = s.readProperty("STATION;name")
                val faction = factions.firstOrNull() { it.name == s.readProperty("STATION;faction") }
                    ?: UNKNOWN_FACTION
                val hangar = s.readSection("HANGAR")
                    .map {
                        val items = it.split(";")
                        val c = items[1].toInt()
                        val modifier = items[2].toDouble()
                        val shipId = ShipID(items[3].substringBefore("."))
                        ShipInHangar(ships[shipId] ?: UNKNOWN_SHIP, modifier, c)
                    }
                val services = s.filter { it.startsWith("SERVICES;") && it.endsWith(";True") }
                    .map { StationService.valueOf(it.substringAfter(";").substringBefore(";")) }
                    .toSet()
                id to Station(
                    id, name, faction, sectors[sectorId] ?: UNKNOWN_SECTOR, false, hangar, listOf(), listOf(), listOf(), services
                )
            } ?: throw IllegalStateException()

        items = File("$dbPath\\items\\items_database.txt")
            .readLines()
            .filter { it.isNotEmpty() && !it.startsWith("//") }
            .associate {
                val item = Item.fromStr(it)
                item.id to item
            }
    }
}

fun printBestMarket(stations: List<Station>, shipCargo: List<PlayerShip>, db: DB) {
//    val allItems = stations.flatMap { station ->
//        station.marketItems.map { it.id }
//    }.toSet()
    println("Demand for items on owned ships:")
    shipCargo.forEach { ship ->
        println(ship.name)
        val list = ship.cargo.map { (itemID, _) ->
            val buy = stations.mapNotNull { station ->
                val found = station.demand.firstOrNull { item -> item.id == itemID }
                if (found != null) (station.name to found.logPrice) else null
            }.sortedBy { it.second }
                .map { (name, price) -> "$name $price" }
            "${db.items[itemID]?.name ?: UNKNOWN_ITEM}: ${buy.joinToString(", ")}"
        }
        list.forEach {
            println("  $it")
        }
    }
}

data class PlayerShip(val name: String, val cargo: List<Pair<ItemID, Int>>) {
    fun toString(db: DB): String {
        return "PlayerShip(name='$name', cargo=${cargo.map { "${it.second} ${db.items[it.first]?.name ?: UNKNOWN_ITEM.name}" }.joinToString(", ")})"
    }
}
data class LogItem(val name: String, val count: Int, val price: Double)

class Logs(savePath: String, db: DB) {
    private val sectorLogs: List<List<String>>
    private val stations: List<Station>
    private val playerShips: List<PlayerShip>

    init {
        val dir = File("$savePath\\journal").listFiles()?.filter { it.name.startsWith("sector_") }
            ?.filterNotNull()
            ?: throw IllegalStateException()
        sectorLogs = dir.map(File::readLines)
        stations = dir.map { file ->
            val lines = file.readLines()
            val sn = lines.first()
                .substringAfter("]")
                .substringAfter("]")
                .substringBefore("[")
            val dbStation = db.stations.values.firstOrNull { it.name == sn }?.copy()

            val station = if (dbStation != null) {
                val demands = lines.readSection("Station Market Orders").drop(1)
                    .map { itemName ->
                        db.items.values.firstOrNull { it.name == itemName } ?: UNKNOWN_ITEM
                    }

                val items = lines.readSection("Station Market Items").drop(1)
                val itemList = items.mapNotNull { itemLine ->
                    val x = itemLine.lastIndexOf("  ")
                    val itemName = itemLine.substring(0 until x)
                    val (itemCount, itemPrice) = itemLine.substring(x + 2).split(" - ")
                    val li = LogItem(
                        itemName, itemCount.trim().toInt(),
                        itemPrice.trim().substring(1).replace(",", "").toDouble()
                    )
                    if (itemName.endsWith(" (Doc)")) {
                        null
                    } else {
                        db.items.values.firstOrNull { it.name == li.name }
                            ?.copy(logCount = li.count, logPrice = li.price)
                            ?: UNKNOWN_ITEM.also { println("unkown item: $itemName") }
                    }
                }
                dbStation.copy(demand = demands, marketItems = itemList)
            } else {
                UNKNOWN_STATION
            }
            station
        }

        val shipdir = File("$savePath\\ships").listFiles()?.filter { it.name.endsWith(".txt") }
            ?.filterNotNull()
            ?: throw IllegalStateException()
        playerShips = shipdir.map { shipFile ->
            val lines = shipFile.readLines()
            val name = lines.readProperty("SHIP_name")
            val cargo = lines.readSection("CARGO").map {
                val (_, amount, id) = it.split(";")
                ItemID(id.trim().toInt()) to amount.trim().toInt()
            }
            PlayerShip(name, cargo)
        }
        printBestMarket(stations, playerShips, db)
    }

    fun getTableData(): Pair<Array<Array<Any>>, Array<String>> {
        val columnTitle = arrayOf(
            "Station",
            "Sector",
            "Ship name",
            "Class",
            "Skill level",
            "Type",
            "Cargo",
            "Active slots",
            "Passive slots",
            "Base Price",
            "Faction"
        )

        val list: MutableList<Array<Any>> = mutableListOf()
        stations.map { (_, stationName, faction, sector, _, hangar, ˍ , _, _, services) ->
            val ships = hangar
//            val ships = hangar.mapNotNull {
//                if (it.ship.toString().contains(filter)) {
//                    it
//                } else {
//                    null
//                }
//            }
            if (ships.isNotEmpty()) {
                ships.map {
                    list.add(
                        arrayOf(
                            stationName + if (!services.contains(StationService.hangar)) " (no hangar!)" else "",
                            sector.name,
                            it.ship.name,
                            it.ship.clazz,
                            it.ship.skillLevel,
                            it.ship.type,
                            it.ship.cargo,
                            it.ship.activeSlots,
                            it.ship.passiveSlots,
//                            it.ship.price * it.modifier * (1.0 - faction.bonusses[16])
                            it.ship.price,
                            "${faction.name} (${faction.score.toString().substring(0..2)})"
                        )
                    )
                }
            }
        }
        return list.toTypedArray() to columnTitle
    }
}

private const val CARGO_COLUMN = 6
private const val PRICE_COLUMN = 9

class MainFrame(private val data: Array<Array<Any>>, private val columnTitle: Array<String>) : JFrame() {
    init {
        title = "Astrox Log Spy"

        createLayout()
        pack()

        defaultCloseOperation = EXIT_ON_CLOSE
        setSize(1200, 1000)
        setLocationRelativeTo(null)
    }

    class TableRenderer : DefaultTableCellRenderer() {
        override fun getTableCellRendererComponent(
            table: JTable?, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int
        ): Component {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column)
            when (value) {
                is Int -> {
                    text = value.formated()
                    horizontalAlignment = JLabel.RIGHT
                }
                is Double -> {
                    text = value.formated()
                    horizontalAlignment = JLabel.RIGHT
                }
                else -> {
                    text = value.toString()
                    horizontalAlignment = JLabel.LEFT
                }
            }
            return this
        }
    }

    private fun createLayout() {
        val model = DefaultTableModel(data, columnTitle)

        val table = JTable(model)
        with(table) {
            autoResizeMode = JTable.AUTO_RESIZE_OFF
            columnModel.getColumn(0).preferredWidth = 200
            columnModel.getColumn(1).preferredWidth = 140
            columnModel.getColumn(2).preferredWidth = 180
            columnModel.getColumn(3).preferredWidth = 100
            columnModel.getColumn(4).preferredWidth = 40
            columnModel.getColumn(5).preferredWidth = 80
            columnModel.getColumn(6).preferredWidth = 50
            columnModel.getColumn(7).preferredWidth = 40
            columnModel.getColumn(8).preferredWidth = 40
            columnModel.getColumn(9).preferredWidth = 90
            columnModel.getColumn(10).preferredWidth = 100
            autoCreateRowSorter = true
            background = Color.DARK_GRAY
            foreground = Color.WHITE
            font = font.deriveFont(14.0f)
            tableHeader.font = font.deriveFont(14.0f)
            rowSorter = TableRowSorter(model).apply {
                setComparator(CARGO_COLUMN) { o1: Any, o2: Any ->
                    (o1 as Int).compareTo(o2 as Int)
                }
                setComparator(PRICE_COLUMN) { o1: Any, o2: Any ->
                    (o1 as Int).compareTo(o2 as Int)
                }
            }
            setDefaultRenderer(Any::class.java, TableRenderer())
            this
        }

        with(getRootPane()) {
            contentPane.add(JScrollPane(table).also {
                it.viewport.background = Color.DARK_GRAY
                it.viewport.foreground = Color.WHITE
            })
        }
    }
}

fun usage(error: String): Nothing {
    val t = StringBuilder("Astrox log spy\n")
    t.appendLine(error)
    t.appendLine()
    t.appendLine("Usage: astroxspy (for standard steam install)")
    t.appendLine("   or: astroxspy \"<name of save>\" (for standard steam install)")
    t.appendLine("   or: astroxspy \"<name of save>\" \"<path to game>\"")
    t.appendLine("Example: java -jar astroxspy \"MySave\"")
    t.appendLine("Example: java -jar astroxspy \"My Save\" \"C:\\Program Files (x86)\\Astrox Imperium\"")

    val frame = JFrame("Astrox Log Spy")
    val panel = JPanel().apply {
        add(JTextArea(t.toString()))
        add(JButton("Ok").apply {
            addActionListener { frame.dispatchEvent(WindowEvent(frame, WindowEvent.WINDOW_CLOSING)) }
        })
    }.apply { layout = BoxLayout(this, BoxLayout.PAGE_AXIS) }
    JDialog(frame, "Astox Log Spy: Initialisation error", true).apply {
        contentPane.add(panel)
        pack()
        defaultCloseOperation = JDialog.DISPOSE_ON_CLOSE
        isVisible = true
    }
    exitProcess(0)
}

fun main(args: Array<String>) {
    if (args.size !in 0..2) {
        usage("Invalid number of parameters! One or two expected but was ${args.size}.")
    }
    val prgPath = if (args.size == 2) {
        args[1].removeSurrounding("\"").removeSuffix("\\")
    } else {
        """C:\Program Files (x86)\Steam\steamapps\common\Astrox Imperium"""
    }
    if (!File(prgPath).exists()) {
        usage("Game folder not found! '$prgPath'")
    }
    val dbPath = """$prgPath\Astrox Imperium_Data\MOD"""
    if (!File(dbPath).exists()) {
        usage("MOD folder inside game folder not found! '$dbPath'")
    }
    val savePath = if (args.isEmpty()) {
        val saves = File("$prgPath\\Astrox Imperium_Data\\MOD\\saves\\")
            .listFiles()?.filter { it.isDirectory }
        if ((saves == null) || saves.isEmpty()) {
            usage("No save file found! '$prgPath\\Astrox Imperium_Data\\MOD\\saves\\'")
        } else if (saves.size == 1) {
            saves.first().path
        } else {
            // show a dialog to select the save to load
            val input = JOptionPane.showInputDialog(
                null, "Please specify the one to use",
                "Multiple save files found",
                JOptionPane.QUESTION_MESSAGE,
                null,  // Use default icon
                saves.map { it.name }.toTypedArray(),
                saves[0] // Initial choice
            ) as String?
                ?: exitProcess(0)
            "$prgPath\\Astrox Imperium_Data\\MOD\\saves\\$input"
        }
    } else {
        prgPath + """\Astrox Imperium_Data\MOD\saves\""" + args[0].removeSurrounding("\"")
    }
    if (!File(savePath).exists()) {
        usage("Save file not found! '$savePath'")
    }

    val (data, columnTitle) = Logs(savePath, DB(dbPath, savePath)).getTableData()
    EventQueue.invokeLater {
        MainFrame(data, columnTitle).isVisible = true
    }
}
